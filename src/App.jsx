import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import Home from './pages/Home'
import { ToastContainer } from 'react-toastify';

export default function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home/>}/>
      </Routes>
      <ToastContainer/>
    </Router>
  )
}