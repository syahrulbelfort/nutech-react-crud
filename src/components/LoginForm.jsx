import { useContext, useState } from 'react';
import { AuthContext } from '../context/AuthContext';

export default function Login2() {
  const { login, isLoggedIn, logout, user } = useContext(AuthContext);
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    await login(username, password);
    setUsername('');
    setPassword('');
  };

  console.log(username)


  const handleLogout = () => {
    logout();
  };

  const handleChangeUsername = (e) => {
    setUsername(e.target.value);
  };

  const handleChangePassword = (e) => {
    setPassword(e.target.value);
  };

  return (
    <div>
      <form
        onSubmit={isLoggedIn ? handleLogout : handleSubmit}
        className="bg-gray-700 rounded-xl px-10  py-5 me-5 w-96 h-full"
      >
        <div className="flex flex-col gap-4">
          {isLoggedIn ? (
            <>
              <h3 className="text-xl text-center text-white font-bold pt-20">
                Hi.. {user.firstName} <br/> Welcome Back !
              </h3>
              <button
                type="submit"
                className="bg-blue-700 w-full mt-3 rounded-lg py-3 px-8 text-white font-semibold"
              >
                Logout
              </button>
            </>
          ) : (
            <>
              <h3 className="text-xl text-white font-bold">Login</h3>
              <p className="text-sm text-gray-400 w-64 mb-2 -mt-2">
                Login untuk melihat data dan  mencoba  fitur lengkap
              </p>
              <label className="text-white font-semibold">Username:</label>
              <input
                className="border-solid py-1 border-2 border-zinc-400 px-2 rounded-lg"
                type="text"
                onChange={handleChangeUsername}
                value={username}
              />
              <label className="font-semibold text-white">Password:</label>
              <input
                className="border-solid py-1 border-2 border-zinc-400 px-2 rounded-lg"
                type="password"
                onChange={handleChangePassword}
                value={password}
              />
              <button
                type="submit"
                className="bg-blue-700 w-full mt-3 rounded-lg py-3 h- px-8 text-white font-semibold"
              >
                Login
              </button>
            </>
          )}
        </div>
      </form>
    </div>
  );
}
