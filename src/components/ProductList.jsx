/* eslint-disable no-unused-vars */
import { useState, useContext } from 'react';
import Popup from 'reactjs-popup';
import ProductEdit from './editProduct';
import { ProductContext } from "../context/ProductContext";
import ReactPaginate from 'react-paginate';
import Search from './Search';
import ReadMe from './ReadMe';
import { AuthContext } from '../context/AuthContext';
import LoginForm from './LoginForm';

export default function ProductList() {
  const { produk, hapusProduk, editProduk } = useContext(ProductContext);
  const { isLoggedIn } = useContext(AuthContext);
  const [isEditOpen, setIsEditOpen] = useState(false);
  const [editedProduk, setEditedProduk] = useState(null);
  const [isSearch, setIsSearch] = useState('');
  const [currentPage, setCurrentPage] = useState(0);
  const [button, setButton] = useState(false);
  const itemsPerPage = 5;

  const handleDelete = (id) => {
    const konfirmasiHapus = window.confirm(
      'Apakah anda yakin ingin menghapus produk ini?'
    );
    if (konfirmasiHapus) {
      hapusProduk(id);
    }
  };

  const handleClick = () => {
    setButton((prevValue) => !prevValue);
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setIsSearch(value);
    setCurrentPage(0);
  };

  const openEditPopup = (data) => {
    setEditedProduk(data);
    setIsEditOpen(true);
  };

  const handleSave = (updatedProduk) => {
    editProduk(updatedProduk.id, updatedProduk);
    setIsEditOpen(false);
  };

  // Pagination
  const indexOfLastItem = (currentPage + 1) * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const pageCount = Math.ceil(produk.length / itemsPerPage);

  const handlePageClick = ({ selected }) => {
    setCurrentPage(selected);
  };


  const tableHead = () => {
    return (
      <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
        <tr>
          <th scope="col" className="px-6 py-3">
            ID
          </th>
          <th scope="col" className="px-6 py-3">
            Foto Produk
          </th>
          <th scope="col" className="px-6 py-3">
            <div className="flex items-center">Nama Produk</div>
          </th>
          <th scope="col" className="px-6 py-3">
            <div className="flex items-center">Harga Beli</div>
          </th>
          <th scope="col" className="px-6 py-3">
            <div className="flex items-center">Harga Jual</div>
          </th>
          <th scope="col" className="px-6 py-3">
            <div className="flex items-center">Qty</div>
          </th>
          <th scope="col" className="px-6 py-3">
            Aksi
          </th>
        </tr>
      </thead>
    );
  };

  const tableBodyFilter = () => {
    const filteredData = produk.filter((data) =>
      data.nama.toLowerCase().includes(isSearch.toLowerCase())
    );

    if (isSearch && filteredData.length === 0) {
      return (
        <tbody className="text-xs mx-auto divide-y divide-gray-200 dark:bg-transparent dark:text-gray-400">
          <tr>
            <td colSpan="7" className="px-6 py-4 text-center">
              No matching data found.
            </td>
          </tr>
        </tbody>
      );
    }

    const currentFilteredItems = filteredData.slice(
      indexOfFirstItem,
      indexOfLastItem
    );

    return (
      <tbody className="text-xs x divide-y divide-gray-200 dark:bg-transparent dark:text-gray-400">
        {currentFilteredItems.map((data) => (
          <tr key={data.id}>
            <td className="px-6 py-4">{data.id}</td>
            <td className="px-6 py-4">
              <img className="w-20" src={data.image} alt="Product" />
            </td>
            <td className="px-6 py-4 w-32 text-white font-semibold">{data.nama}</td>
            <td className="px-6 py-4">Rp {data.hargaBeli}</td>
            <td className="px-6 py-4">Rp {data.hargaJual}</td>
            <td className="px-6 py-4"> {data.stok}pcs</td>
            <td className="px-6 py-4">
              <button
                onClick={() => openEditPopup(data)}
                className="mr-2 text-blue-600 hover:text-indigo-900"
              >
                Edit
              </button>
              <button
                onClick={() => handleDelete(data.id)}
                className="text-red-600 hover:text-red-900"
              >
                Hapus
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    );
  };

  const formEdit = () => {
    return (
      <Popup open={isEditOpen} onClose={() => setIsEditOpen(false)}>
        <ProductEdit data={editedProduk} onSave={handleSave} />
      </Popup>
    );
  };

  return (
    <>
      <div className="relative overflow-x-auto  sm:rounded-lg">
        <div className="flex justify-between mb-2 items-center">
          <Search isSearch={isSearch} handleSearch={handleChange} />
          <ReadMe handleClick={handleClick} />
        </div>
        <div className="flex">
          <LoginForm/> 
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          {tableHead()}
        {isLoggedIn ? (
          <>
            {tableBodyFilter()}
          </>
            ) : <p></p>
          }
            </table>
        {formEdit()}
        </div>

          {isLoggedIn &&
                  <div className="flex justify-center mt-4 ms-96">
                  <ReactPaginate
                    className="flex text-white text-center gap-20"
                    previousLabel={'Previous'}
                    nextLabel={'Next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={handlePageClick}
                    containerClassName={'pagination'}
                    activeClassName={'active'}
                  />
                </div>
          }

      </div>
    </>
  );
}
