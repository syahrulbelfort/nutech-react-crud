/* eslint-disable react/prop-types */
import Popup from 'reactjs-popup';
import YouTube from 'react-youtube';

export default function ReadMe({ handleClick }) {
  const onPlayerReady = (event) => {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  };

  const opts = {
    height: '200',
    width: '390',
    playerVars: {
      autoplay: 1,
    },
  };

  return (
    <Popup
      trigger={
        <button
          onClick={handleClick}
          className="text-white animate-bounce bg-blue-700 hover:bg-blue-800  font-medium rounded-lg text-sm px-5 py-2.5 h-10 text-center dark:bg-blue-600"
        >   
        Mohon dibaca!
        </button>
      }
      modal
      nested
    >
      {(close) => (
        <div className="modal">
          <button className="close" onClick={close}>
            &times;
          </button>
          <div className="content ">
            <div className="bg-gray-700 rounded-xl p-10">
              <h3 className="text-xl text-white font-bold">Instruksi memulai</h3>

              <p className="text-sm text-white mt-2 text-lg font-semibold ">
                    NOTES :
              </p>
              <p className="text-sm text-gray-400 w-96 mt-2 mb-4">
                Silahkan login terlebih dahulu untuk melihat data dan mencoba semua feature! <br/><br/>
                <span className='font-semibold text-white'>Tambah produk </span>: harus diisi semua kolom pastikan gambar di bawah 100kb dan sudah login.              
              </p>

              <p className="text-sm text-white mt-2 text-lg font-semibold ">
                Login dengan username & password dibawah ini :
                
              </p>
              <p className="text-sm text-gray-400 w-96 mt-2 mb-4">
              username  : atuny0 <br/>
              password  : 9uQFF1Lh
              </p>
              <p className="text-sm text-white text-lg font-semibold ">
                    Demo :
              </p>
              <YouTube videoId="Ot4aDCQ4KyQ" className=' mt-4' opts={opts} onReady={onPlayerReady} />
            </div>
          </div>
        </div>
      )}
    </Popup>
  );
}
