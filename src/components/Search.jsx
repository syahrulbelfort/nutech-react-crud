import {BiSearchAlt} from 'react-icons/bi'
/* eslint-disable react/prop-types */

export default function Search({ handleSearch, isSearch}) {

  return (
    <div className="mb-4 flex mt-4 rounded-lg gap-4 items-center bg-white">
      <BiSearchAlt
      className='text-black ms-2 h-8 w-8'
      />
      <input
        type="text"
        placeholder="Search"
        value={isSearch}
        onChange={handleSearch}
        className="px-2  py-2 border-0 border-gray-300 w-80 rounded-md focus:outline-none "
      />
    </div>
  );
}
