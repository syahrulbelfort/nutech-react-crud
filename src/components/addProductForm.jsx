import { useContext, useState } from "react";
import Popup from 'reactjs-popup';
import { ProductContext } from "../context/ProductContext";
import { toast } from "react-toastify";
import { AuthContext } from "../context/AuthContext";

export default function ProductForm() {
  const [namaProduk, setNamaProduk] = useState('');
  const [hargaBeli, setHargaBeli] = useState('');
  const [hargaJual, setHargaJual] = useState('');
  const [stok, setStok] = useState('');
  const [imageProduk, setImageProduk] = useState(null);

  const { tambahProduk } = useContext(ProductContext);
  const { isLoggedIn } = useContext(AuthContext)

  const handleProductChange = (e) => {
    const productValue = e.target.value;
    setNamaProduk(productValue);
  };

  const handleHargaBeliChange = (e) => {
    const hargaBeliValue = e.target.value;
    setHargaBeli(hargaBeliValue);
  };

  const handleHargaJualChange = (e) => {
    const hargaJualValue = e.target.value;
    setHargaJual(hargaJualValue);
  };

  const handleStokChange = (e) => {
    const stokValue = e.target.value;
    setStok(stokValue);
  };

  const handleImageChange = (e) => {
    const file = e.target.files[0];

    if (file && file.type !== 'image/jpeg' && file.type !== 'image/png') {
      toast.error('File harus berformat JPG / PNG.', {
        position: 'top-center'
      },
      setImageProduk('')
      );
    }

    if (file && file.size > 100 * 1024) {
      toast.error('Ukuran file tidak boleh melibihi 100KB.', {
        position: 'top-center'
      },
      setImageProduk('')
      );
      return;
    }

    setImageProduk(URL.createObjectURL(file));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!namaProduk || !hargaBeli || !hargaJual || !stok || !imageProduk || !isLoggedIn) {
      toast.error('Mohon login & isi semua kolom!', {
        position: 'top-center'
      },
      );
      return;
    }

    const newProduct = {
      id: Date.now().toString(),
      image: imageProduk,
      nama: namaProduk,
      hargaBeli: hargaBeli,
      hargaJual: hargaJual,
      stok: stok
    };

    tambahProduk(newProduct);

    setImageProduk(null);
    setNamaProduk('');
    setHargaBeli('');
    setHargaJual('');
    setStok('');
  };

  const handleClick = () => {
    if (!isLoggedIn) {
      toast.error('Login terlebih dahulu untuk melihat produk!', {
        position: 'top-center'
      });
    }
    
  };

  return (
    <div className="relative overflow-x-auto sm:rounded-lg">
      <h2 className="text-3xl font-bold text-white text-center">Daftar Produk</h2>
      <Popup
        trigger={<button
          onClick={handleClick}
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5
        text-center dark:bg-blue-600 dark:focus:ring-blue-800">+
        </button>}
        modal
        nested
      >
        {close => (
          <div className="modal">
            <button className="closeAdd" onClick={close}>
              &times;
            </button>
            <div className="content">
              <form onSubmit={handleSubmit} className="bg-gray-700 rounded-xl h-100 p-10 -mt-10 ">
                <div className="flex flex-col gap-3">
                  <h3 className="text-xl text-white font-bold pt-4">Info Produk</h3>
                  <p className="text-sm text-gray-400 w-96 mb-2 mt-2">
                    Informasi Produk Pastikan produk tidak melanggar Hak Kekayaan Intelektual
                  </p>
                  <label className="text-white">Upload Foto :</label>
                  <input
                    className="block w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-blue-700 file:text-white"
                    type="file"
                    accept="image/jpeg, image/png"
                    onChange={handleImageChange}
                  />
                  <label className="text-white font-semibold">Nama Produk :</label>
                  <input
                    className="border-solid py-1 border-2 border-zinc-400 px-2 rounded-lg"
                    type="text"
                    onChange={handleProductChange}
                    value={namaProduk}
                  />
                  <label className="font-semibold text-white">Harga Beli :</label>
                  <input
                    className="border-solid py-1 border-2 border-zinc-400 px-2 rounded-lg"
                    type="number"
                    onChange={handleHargaBeliChange}
                    value={hargaBeli}
                  />
                  <label className="text-white">Harga Jual :</label>
                  <input
                    className="border-solid border-2 py-1 border-zinc-400 px-2 rounded-lg"
                    type="number"
                    onChange={handleHargaJualChange}
                    value={hargaJual}
                  />
                  <label className="text-white font-semibold">Stok :</label>
                  <input
                    className="border-solid border-2 py-1 border-zinc-400 px-2 rounded-lg"
                    type="number"
                    onChange={handleStokChange}
                    value={stok}
                  />
                  <button
                    type="submit"
                    className="bg-blue-700 w-full mt-3 rounded-lg py-3 px-8 text-white font-semibold"
                  >
                    Tambah produk
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </Popup>
    </div>
  );
}
