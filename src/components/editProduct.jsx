/* eslint-disable react/prop-types */
import { useState } from 'react';
import { toast } from 'react-toastify';

export default function ProductEdit({ data, onSave }) {
  const [editedProduk, setEditedProduk] = useState(data);
  const [isClose, setIsClose] = useState(true)

  const handleSubmit = (e) => {
    e.preventDefault();
    onSave(editedProduk);
    toast.success('Produk berhasil diupdate!', {
      position : 'top-center'
    })
  };

  const editProduk = (field, value) => {
    setEditedProduk((prevState) => ({
      ...prevState,
      [field]: value,
    }));
 
  };

  const close = ()=>{
    setIsClose(prevValue => !prevValue)
  }

  return (
    <div>
      {isClose && (
        <div className="modal">
          <button className="close" onClick={close}>
            &times;
          </button>
          <form onSubmit={handleSubmit} className="bg-gray-700 rounded-xl p-16">
            <h3 className="text-xl text-white font-bold">Edit Produk</h3>
            <p className="text-sm text-gray-400 w-96 mt-2 mb-4">
              Informasi Produk. Pastikan produk tidak melanggar Hak Kekayaan Intelektual.
            </p>
            <div className="flex flex-col mb-4">
              <label className="mb-1 text-white font-semibold">Nama Produk</label>
              <input
                type="text"
                value={editedProduk.nama}
                onChange={(e) => editProduk('nama', e.target.value)}
                className="border border-gray-300 rounded px-2 py-1"
              />
            </div>
            <div className="flex flex-col mb-4">
              <label className="mb-1 text-white font-semibold">Harga Beli</label>
              <input
                type="text"
                value={editedProduk.hargaBeli}
                onChange={(e) => editProduk('hargaBeli', e.target.value)}
                className="border border-gray-300 rounded px-2 py-1"
              />
            </div>
            <div className="flex flex-col mb-4">
              <label className="mb-1 text-white font-semibold">Harga Jual</label>
              <input
                type="text"
                value={editedProduk.hargaJual}
                onChange={(e) => editProduk('hargaJual', e.target.value)}
                className="border border-gray-300 rounded px-2 py-1"
              />
            </div>
            <div className="flex flex-col mb-4">
              <label className="mb-1 text-white font-semibold">Harga Jual</label>
              <input
                type="text"
                value={editedProduk.stok}
                onChange={(e) => editProduk('stok', e.target.value)}
                className="border border-gray-300 rounded px-2 py-1"
              />
            </div>
            <button
              type="submit"
              className="bg-blue-700 w-full text-white font-semibold py-2 px-4 rounded"
            >
              Simpan
            </button>
          </form>
        </div>
      )}
    </div>
  );
}
