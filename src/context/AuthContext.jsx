/* eslint-disable react/prop-types */
import { createContext, useState, useEffect } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [token, setToken] = useState('');
  const [user, setUser] = useState(null);

  useEffect(() => {
    const storedToken = localStorage.getItem('token');
    if (storedToken) {
      setToken(storedToken);
      setIsLoggedIn(true);
      const parsedUser = JSON.parse(atob(storedToken.split('.')[1]));
      setUser(parsedUser);
    }
  }, []);

  const handleLogin = async (username, password) => {
    try {
      const response = await axios.post('https://dummyjson.com/auth/login', {
        username,
        password,
      });
      const { token } = response.data;
      localStorage.setItem('token', token);
      setToken(token);
      const parsedUser = JSON.parse(atob(token.split('.')[1]));
      setUser(parsedUser);
      setIsLoggedIn(true);
      toast.success('Login berhasil!', {
        position: 'top-center',
      });
    } catch (error) {
      console.error('Error logging in:', error);
      toast.error('Gagal melakukan login!', {
        position: 'top-center',
      });
    }
  };

  const handleLogout = () => {
    localStorage.removeItem('token');
    setToken('');
    setIsLoggedIn(false);
    setUser(null);
    toast.error('Logout berhasil!', {
      position: 'top-center',
    });
  };

  return (
    <AuthContext.Provider value={{ isLoggedIn, token, user, login: handleLogin, logout: handleLogout }}>
      {children}
    </AuthContext.Provider>
  );
};
