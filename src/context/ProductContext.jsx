/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import { createContext, useEffect, useState } from "react";
import axios from "axios";
import { toast } from "react-toastify";

export const ProductContext = createContext();

export const ProductProvider = ({ children }) => {
  const [produk, setProduk] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("https://648aa5cb17f1536d65e964fc.mockapi.io/api/v1/produk");
        const sortedData = response.data.sort((a, b) => {
          const dateA = new Date(a.updatedAt);
          const dateB = new Date(b.updatedAt);
          return dateB - dateA;
        });

        setProduk(sortedData.reverse());
        localStorage.setItem("produk", JSON.stringify(sortedData));
      } catch (error) {
        console.log("Error fetching data from JSON Server:", error);
      }
    };
    fetchData();
  }, []);

  const tambahProduk = async (barang) => {
    const produkSudahTersedia = produk.find((p) => p.nama === barang.nama);
    if (produkSudahTersedia) {
      toast.error("Nama produk harus unik", {
        position: "top-center",
      });
    } else {
      try {
        const response = await axios.post("https://648aa5cb17f1536d65e964fc.mockapi.io/api/v1/produk", barang);
        setProduk((prevProduk) => [response.data, ...prevProduk]);
        toast.success("Produk berhasil ditambahkan!", {
          position: "top-center",
        });

        // Update the local storage with the updated produk array
        localStorage.setItem(
          "produk",
          JSON.stringify([response.data, ...produk])
        );
      } catch (error) {
        console.log("Error adding product:", error);
      }
    }
  };

  const hapusProduk = async (id) => {
    try {
      await axios.delete(`https://648aa5cb17f1536d65e964fc.mockapi.io/api/v1/produk/${id}`);
      setProduk((prevProduk) =>
        prevProduk.filter((barang) => barang.id !== id)
      );
      toast.error("Produk berhasil dihapus!", {
        position: "top-center",
      });

      localStorage.setItem(
        "produk",
        JSON.stringify(produk.filter((barang) => barang.id !== id))
      );
    } catch (error) {
      console.log("Error deleting product:", error);
    }
  };

  const editProduk = async (id, updatedProduk) => {
    try {
      await axios.put(`https://648aa5cb17f1536d65e964fc.mockapi.io/api/v1/produk/${id}`, updatedProduk);
      setProduk((prevProduk) =>
        prevProduk.map((barang) => (barang.id === id ? updatedProduk : barang))
      );

      // Update the local storage with the updated produk array
      localStorage.setItem(
        "produk",
        JSON.stringify(
          prevProduk.map((barang) =>
            barang.id === id ? updatedProduk : barang
          )
        )
      );
    } catch (error) {
      console.log("Error updating product:", error);
    }
  };

  useEffect(() => {
    const savedProduk = JSON.parse(localStorage.getItem("produk"));
    if (savedProduk) {
      setProduk(savedProduk);
    }
  }, []);

  return (
    <ProductContext.Provider
      value={{
        produk,
        tambahProduk,
        hapusProduk,
        editProduk,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};
