import ProductForm from "../components/addProductForm";
import ProductList from "../components/ProductList";
import { ProductProvider } from "../context/ProductContext";
import { AuthProvider } from "../context/AuthContext";
import 'react-toastify/dist/ReactToastify.css';

export default function Home() {
  return (
    <div className="sm:max-w-lg md:max-w-lg lg:max-w-6xl lg:grid  mx-auto mt-10 mb-10">
      <AuthProvider>
        <ProductProvider>
          <ProductForm />
          <ProductList />
        </ProductProvider>
      </AuthProvider>
    </div>
  );
}
